This subgroup is to host work done in Celduin around the subject of Remote
Execution. We aim to do most of our work upstream, so this repository is
primarily a place to explain our high level aims and gitlab issues not suited
for upstream projects (i.e. exploration into compatibilities, devops).

# Our High Level Goals

At the moment, our top level goal is to improve the compatibility of BuildBarn
with various Remote Execution clients. In particular, we want to get first
class support for Bazel, BuildStream and RECC or Goma.
